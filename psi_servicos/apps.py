from django.apps import AppConfig


class PsiServicosConfig(AppConfig):
    name = 'psi_servicos'
