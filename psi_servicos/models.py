from django.db import models
# from .constants import *

class Servico(models.Model):
    nome = models.CharField(max_length=255, null=False, blank=False)
    vagas = models.IntegerField()

    class Meta:
        """
        """
        verbose_name_plural = 'Atendimentos'

    def __str__(self):
        return f'{self.nome} - {self.vagas}'
