# users/urls.py
from django.urls import include, path
from . import views

urlpatterns = [
    path('servicos', views.ServicoListView.as_view()),
    path('servico/<int:pk>', views.ServicoDetail.as_view())
]