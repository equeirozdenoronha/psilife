# users/views.py
from rest_framework import generics
from psi_servicos.models import Servico
from . import serializers
from .serializers import ServicoSerializer


class ServicoListView(generics.ListCreateAPIView):
    queryset = Servico.objects.all()
    serializer_class = serializers.ServicoSerializer


class ServicoDetail(generics.RetrieveUpdateDestroyAPIView):
    queryset = Servico.objects.all()
    serializer_class = serializers.ServicoSerializer

