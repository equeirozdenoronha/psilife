from rest_framework import serializers
from . import models


class ServicoSerializer(serializers.ModelSerializer):

    class Meta:
        model = models.Servico
        fields = '__all__'

    def create(self, validated_data):
        servico = models.Servico.objects.create_user(**validated_data)
        return servico