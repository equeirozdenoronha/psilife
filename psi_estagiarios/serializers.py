# users/serializers.py
from rest_framework import serializers
from psi_estagiarios.models import *


class EstagiarioSerializer(serializers.ModelSerializer):
    class Meta:
        model = Estagiario
        fields = '__all__'

    def create(self, validated_data):
        estagiario = Estagiario.objects.create(**validated_data)
        return estagiario


class PontoSerializer(serializers.ModelSerializer):
    class Meta:
        model = Ponto
        fields = '__all__'

    def create(self, validated_data):
        ponto = Ponto.objects.create(**validated_data)
        return ponto

class ListaPontoSerializer(serializers.ModelSerializer):
    estagiario = serializers.SlugRelatedField(read_only=True, slug_field='nome')
    class Meta:
        model = Ponto
        fields = '__all__'

    def create(self, validated_data):
        ponto = Ponto.objects.create(**validated_data)
        return ponto