# users/urls.py
from django.urls import include, path

from . import views

urlpatterns = [
    path('estagiarios', views.EstagiarioListCreateView.as_view()),
    path('estagiario/<int:pk>', views.EstagiarioDetailView.as_view()),
    path('cadastra-ponto', views.PontoCreateView.as_view()),
    path('lista-ponto', views.PontoList.as_view()),
    path('ponto/<int:pk>', views.PontoDetailView.as_view()),
    path('ponto-estagiario/<cgu>', views.PontoListView.as_view()),
]