# Generated by Django 2.1.2 on 2019-05-27 19:45

from django.db import migrations, models
import django.db.models.deletion


class Migration(migrations.Migration):

    dependencies = [
        ('psi_estagiarios', '0001_initial'),
    ]

    operations = [
        migrations.CreateModel(
            name='Ponto',
            fields=[
                ('id', models.AutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('dataHoraEntrada', models.DateTimeField(blank=True, null=True)),
                ('dataHoraSaida', models.DateTimeField(blank=True, null=True)),
                ('estagiario', models.ForeignKey(on_delete=django.db.models.deletion.CASCADE, to='psi_estagiarios.Estagiario')),
            ],
        ),
    ]
