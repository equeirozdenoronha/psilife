from django.db import models

from django.db import models
from .constants import *


class Estagiario(models.Model):
    GENEROS = (
        ('F', 'Feminino'),
        ('M', 'Masculino')
    )
    FAIXAS_ETARIAS = (
        ('Criança', 'Criança'),
        ('Adolescente', 'Adolescente'),
        ('Jovem', 'Jovem'),
        ('Adulto', 'Adulto'),
        ('Idoso', 'Idoso'),
    )
    ESTADO_CIVIL = (
        ('Solteiro(a)', 'Solteiro(a)'),
        ('Casado(a)', 'Casado(a)'),
        ('Divorciado(a)', 'Divorciado(a)'),
        ('Viúvo(a)', 'Viúvo(a)'),
        ('Separado(a)', 'Separado(a)'),
    )
    nome = models.CharField(max_length=255)
    cgu = models.CharField(('CGU'), max_length=9, unique=True)
    data_de_nascimento = models.DateField(null=True, blank=True)
    sexo = models.CharField(max_length=1, choices=GENEROS, null=True, blank=True)
    cpf = models.CharField(max_length=14, null=True, blank=True, verbose_name='CPF')
    rg = models.CharField(max_length=12, null=True, blank=True, verbose_name='RG')
    endereco = models.CharField(max_length=255, null=True, blank=True, verbose_name='Endereço')
    cidade = models.CharField(max_length=128, null=True, blank=True)
    uf = models.CharField(max_length=2, choices=ESTADOS.choices(), null=True, blank=True, verbose_name='UF')
    cep = models.CharField(max_length=10, null=True, blank=True, verbose_name='CEP')
    email = models.EmailField(null=True, blank=True, verbose_name='E-mail')
    telefone_comercial = models.CharField(max_length=15, null=True, blank=True)
    telefone_residencial = models.CharField(max_length=15, null=True, blank=True)
    telefone_celular = models.CharField(max_length=15, null=True, blank=True)
    estado_civil = models.CharField(choices=ESTADO_CIVIL, max_length=20, null=True, blank=True)
    qtdHoras = models.FloatField(blank=True, null=True)
    class Meta:
        verbose_name_plural = 'Estagiários'

    def __str__(self):
        return self.nome


class Ponto(models.Model):

    estagiario = models.ForeignKey(Estagiario, on_delete=models.CASCADE)
    dataHoraEntrada = models.CharField(max_length=80, null=True, blank=True)
    dataHoraSaida = models.CharField(max_length=80, null=True, blank=True)


    class Meta:
        verbose_name_plural='Pontos'

    def __str__(self):
        return 'Estagiário:{}, Entrada: {}, Saida: {}'.format(self.estagiario, self.dataHoraEntrada, self.dataHoraSaida)




