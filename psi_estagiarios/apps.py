from django.apps import AppConfig


class PsiEstagiariosConfig(AppConfig):
    name = 'psi_estagiarios'
