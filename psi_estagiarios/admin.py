from django.contrib import admin
# Register your models here.

from psi_estagiarios.models import Estagiario
from psi_estagiarios.models import Ponto


admin.site.register(Estagiario)
admin.site.register(Ponto)