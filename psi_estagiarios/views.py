from django.shortcuts import render
from rest_framework import generics
from psi_estagiarios.models import Estagiario
from psi_estagiarios.models import Ponto
from psi_estagiarios.serializers import *
from rest_framework.response import Response
from rest_framework import status
from rest_framework import filters

class EstagiarioListCreateView(generics.ListCreateAPIView):
    queryset = Estagiario.objects.all()
    serializer_class = EstagiarioSerializer

class EstagiarioDetailView(generics.RetrieveUpdateDestroyAPIView):
    queryset = Estagiario.objects.all()
    serializer_class = EstagiarioSerializer

class PontoCreateView(generics.CreateAPIView):
    queryset = Ponto.objects.all()
    serializer_class = PontoSerializer

class PontoList(generics.ListAPIView):
    queryset = Ponto.objects.all()
    serializer_class = ListaPontoSerializer

class PontoDetailView(generics.RetrieveUpdateDestroyAPIView):
    queryset = Ponto.objects.all()
    serializer_class = PontoSerializer

class PontoListView(generics.ListAPIView):
    serializer_class = ListaPontoSerializer
    def get_queryset(self):
        """
        This view should return a list of all the purchases for
        the estagiarios as determined by the cgu portion of the URL.
        """
        cgu = self.kwargs['cgu']
        return Ponto.objects.filter(estagiario__cgu=cgu)