# api/urls.py
from django.urls import include, path
from django.urls import path
from rest_framework_simplejwt import views as jwt_views
from psi_usuarios import views

urlpatterns = [
    path('users/', include('psi_usuarios.urls')),
    path('acolhidos/', include('psi_acolhidos.urls')),
    path('estagiarios/', include('psi_estagiarios.urls')),
    path('atendimentos/', include('psi_atendimento.urls')),
    path('servicos/', include('psi_servicos.urls')),
    path('login/', views.CustomAuthToken.as_view()),
    path('login-jwt/', jwt_views.TokenObtainPairView.as_view(), name='token_obtain_pair'),
    path('refresh-token/', jwt_views.TokenRefreshView.as_view(), name='token_refresh'),
    path('rest-auth/', include('rest_auth.urls')),
]