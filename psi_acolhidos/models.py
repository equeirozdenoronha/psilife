from django.db import models
from .constants import *


class Acolhido(models.Model):
    GENEROS = (
        ('F', 'Feminino'),
        ('M', 'Masculino')
    )
    FAIXAS_ETARIAS = (
        ('Criança', 'Criança'),
        ('Adolescente', 'Adolescente'),
        ('Jovem', 'Jovem'),
        ('Adulto', 'Adulto'),
        ('Idoso', 'Idoso'),
    )
    ESTADO_CIVIL = (
        ('Solteiro(a)', 'Solteiro(a)'),
        ('Casado(a)', 'Casado(a)'),
        ('Divorciado(a)', 'Divorciado(a)'),
        ('Viúvo(a)', 'Viúvo(a)'),
        ('Separado(a)', 'Separado(a)'),
    )
    nome = models.CharField(max_length=255)
    data_de_nascimento = models.DateField(null=True, blank=True)
    sexo = models.CharField(max_length=1, choices=GENEROS)
    naturalidade_cidade = models.CharField(max_length=128, null=True, blank=True, verbose_name='Naturalidade')
    naturalidade_uf = models.CharField(choices=ESTADOS.choices(), max_length=2, null=True, blank=True, verbose_name='Naturalidade - UF')
    cpf = models.CharField(max_length=14, unique=True, null=True, blank=True, verbose_name='CPF')
    rg = models.CharField(max_length=12, null=True, blank=True, verbose_name='RG')
    faixa_etaria = models.CharField(choices=FAIXAS_ETARIAS, max_length=20, null=True, blank=True, verbose_name='Faixa Etária')
    escolaridade = models.CharField(choices=NIVEIS_DE_ENSINO.choices(), max_length=20, null=True, blank=True, verbose_name='Nível de Escolaridade')
    endereco = models.CharField(max_length=255, null=True, blank=True, verbose_name='Endereço')
    estado_civil = models.CharField(choices=ESTADO_CIVIL, max_length=20, null=True, blank=True)
    nome_da_mae = models.CharField(max_length=255, null=True, blank=True, verbose_name='Nome da mãe')
    cidade = models.CharField(max_length=128, null=True, blank=True)
    uf = models.CharField(max_length=2, choices=ESTADOS.choices(), null=True, blank=True, verbose_name='UF')
    cep = models.CharField(max_length=10, null=True, blank=True, verbose_name='CEP')
    email = models.EmailField(null=True, blank=True, verbose_name='E-mail')
    telefone_comercial = models.CharField(max_length=15, null=True, blank=True)
    telefone_residencial = models.CharField(max_length=15, null=True, blank=True)
    telefone_celular = models.CharField(max_length=15, null=True, blank=True)
    procedencia_indicacao = models.CharField(max_length=30, null=True, blank=True)
    created_at = models.DateTimeField(auto_now_add=True, null=True, blank=True)
    updated_at = models.DateTimeField(null=True, blank=True)

    class Meta:
        verbose_name_plural = 'Acolhidos'

    def __str__(self):
        return self.nome

    # def save(self, *args, **kwargs):
    #     self.nome.upper()
    #     self.cidade.upper()
    #     self.uf.upper()
    #     self.naturalidade_cidade.upper()
    #     self.naturalidade_uf.upper()
    #     super(Acolhido).save(*args, **kwargs)

