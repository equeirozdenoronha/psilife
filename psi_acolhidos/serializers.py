# users/serializers.py
from rest_framework import serializers
from psi_acolhidos.models import *

class AcolhidoSerializer(serializers.ModelSerializer):
    class Meta:
        model = Acolhido
        fields = '__all__'

    def create(self, validated_data):

        return Acolhido.objects.create(**validated_data)