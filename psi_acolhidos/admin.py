from django.contrib import admin
from django.contrib.auth import get_user_model
from django.contrib.auth.admin import UserAdmin
from psi_acolhidos.models import Acolhido
admin.site.register(Acolhido)