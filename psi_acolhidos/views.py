# users/views.py
from rest_framework import generics
from .serializers import *
from rest_framework import status
from rest_framework.decorators import api_view
from rest_framework.response import Response
from psi_acolhidos.models import Acolhido
from psi_acolhidos.serializers import AcolhidoSerializer

class AcolhidoListView(generics.ListCreateAPIView):
    queryset = Acolhido.objects.all()
    serializer_class = AcolhidoSerializer

class AcolhidoDetailView(generics.RetrieveUpdateDestroyAPIView):
    queryset = Acolhido.objects.all()
    serializer_class = AcolhidoSerializer
    # lookup_field = 'cpf'
    # filter_fields = ('cpf', 'nome')
    # search_fields = ('nome', 'cpf', 'naturalidade_cidade')
