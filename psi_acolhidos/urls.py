# users/urls.py
from django.urls import include, path
from . import views

urlpatterns = [
    path('acolhidos', views.AcolhidoListView.as_view()),
    path('acolhido/<int:pk>', views.AcolhidoDetailView.as_view())
]