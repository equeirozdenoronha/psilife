from django.db import models
from .constants import *
from psi_acolhidos import models as acolhidos
from psi_estagiarios import models as estagiarios
from psi_servicos import models as servicos
class Atendimento(models.Model):

    acolhido = models.ForeignKey(acolhidos.Acolhido, on_delete=models.CASCADE, null=False)
    estagiario = models.ForeignKey(estagiarios.Estagiario, on_delete=models.CASCADE, null=False)
    servico = models.ForeignKey(servicos.Servico, on_delete=models.CASCADE, null=False)
    dias_da_semana = models.CharField(max_length=80, blank=True, null=True)
    horario = models.TimeField(null=True, blank=True, verbose_name='Horário do Atendimento')
    sala = models.CharField(max_length=4, null=True, blank=True, verbose_name='Sala de Atendimento')
    is_active = models.BooleanField(null=True, blank=True, default=True)
    created_at = models.DateTimeField(auto_now_add=True, null=True, blank=True)
    updated_at = models.CharField(max_length=80, null=True, blank=True)

    class Meta:
        """
        """
        verbose_name_plural = 'Atendimentos'

    def __str__(self):
        return f'{self.acolhido} - {self.estagiario} - {self.servico}'


    # def save(self, *args, **kwargs):
    #     self.servico.vagas -= 1
    #     super(Atendimento).save(*args, **kwargs)