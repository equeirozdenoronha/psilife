# users/urls.py
from django.urls import include, path

from . import views

urlpatterns = [
    path('atendimentos', views.AtendimentoListView.as_view()),
    path('atendimentos_lista', views.AtendimentoListViewVisualizar.as_view()),
    path('atendimento/<int:pk>', views.AtendimentoDetailView.as_view())
]