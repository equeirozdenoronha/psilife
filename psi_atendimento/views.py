# users/views.py
from rest_framework import generics
from .serializers import *
from .models import Atendimento
from psi_atendimento.serializers import AtendimentoSerializer
import requests
from django.utils.decorators import method_decorator
from django.views.decorators.cache import cache_page
from django.views.decorators.csrf import csrf_exempt
from rest_framework.decorators import action
from rest_framework.response import Response
from rest_framework.viewsets import GenericViewSet


class AtendimentoListView(generics.ListCreateAPIView):
    queryset = Atendimento.objects.all()
    serializer_class = AtendimentoSerializer

class AtendimentoDetailView(generics.RetrieveUpdateDestroyAPIView):
    queryset = Atendimento.objects.all()
    serializer_class = AtendimentoListViewSerializer

class AtendimentoListViewVisualizar(generics.ListCreateAPIView):
    queryset = Atendimento.objects.all()
    serializer_class = AtendimentoListViewSerializer
