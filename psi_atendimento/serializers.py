# users/serializers.py
from rest_framework import serializers
from psi_atendimento.models import *

class AtendimentoSerializer(serializers.ModelSerializer):

    # acolhido = serializers.SlugRelatedField(read_only=True, slug_field='nome')
    # estagiario = serializers.SlugRelatedField(read_only=True,slug_field='nome')
    # servico = serializers.SlugRelatedField(read_only=True,slug_field='nome')

    class Meta:
        model = Atendimento
        fields = '__all__'

    def create(self, validated_data):

        return Atendimento.objects.create(**validated_data)

class AtendimentoListViewSerializer(serializers.ModelSerializer):

    acolhido = serializers.SlugRelatedField(read_only=True, slug_field='nome')
    estagiario = serializers.SlugRelatedField(read_only=True,slug_field='nome')
    servico = serializers.SlugRelatedField(read_only=True,slug_field='nome')

    class Meta:
        model = Atendimento
        fields = '__all__'

    def create(self, validated_data):

        return Atendimento.objects.create(**validated_data)