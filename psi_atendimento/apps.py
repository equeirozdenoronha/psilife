from django.apps import AppConfig


class PsiAtendimentoConfig(AppConfig):
    name = 'psi_atendimento'
