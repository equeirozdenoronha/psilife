from django.apps import AppConfig


class PsiUsuariosConfig(AppConfig):
    name = 'psi_usuarios'
