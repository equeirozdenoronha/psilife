# users/urls.py
from django.urls import include, path

from . import views

urlpatterns = [

    path('', views.UserListView.as_view()),
    path('create-user', views.user_create)
]